var head  = document.getElementsByTagName('head')[0];

var brightcove = document.createElement("script");
brightcove.type = "text/javascript";
brightcove.src = '//admin.brightcove.com/js/BrightcoveExperiences_all.js';
head.appendChild(brightcove);

var styles  = document.createElement('link');
styles.rel  = 'stylesheet';
styles.type = 'text/css';
styles.href = '//d40a5d4f9342b88214ae-ff644209e129d3e2766bb57251455d23.ssl.cf1.rackcdn.com/com-styles.min.css';
styles.media = 'all';
head.appendChild(styles);

var banner = '<div id="banner-wrap"><div id="logo-link"></div><div id="child-info-left"></div><div id="child-image"></div><div id="child-info-right"></div><div id="sponsor-button"></div></div><!-- .#banner-wrap --><div id="banner-hover" class="banner-modal"><div class="compassion-logo"></div><div class="blue-corner"></div><span class="close-banner">Close [x]</span><div class="tb-border"><div class="lr-border"><div id="video"></div><div id="child-info"></div><div id="modal-sponsor-button"></div></div><!-- .lr-border --></div><!-- .tb-border --></div><!-- banner-hover -->';
$(banner).appendTo('#compassion-banner1234');

//  VARIABLES
var BC_player,
    BC_videoPlayerModule;
var video_is_ready = false;
var he_or_she = 'He';
var him_or_her = 'Him';

function onTemplateLoaded(expID) {
    try {
        BC_player = brightcove.getExperience(expID);
        bc_VID_ID=expID;
        BC_videoPlayerModule = BC_player.getModule(APIModules.VIDEO_PLAYER);

    } catch (err) {
        console.log(err);
    }
}
function onTemplateReady(evt) {
    try {
        BC_videoPlayerModule.mute(true);
        BC_videoPlayerModule.pause();
        BC_videoPlayerModule.showVolumeControls(true);
        jQuery('#banner-wrap').addClass('ready');
        video_is_ready = true;
    } catch (err) {
       console.log(err);
   }
}
function onTemplateReadyModal(evt) {
    try {
        BC_videoPlayerModule.mute(true);
        BC_videoPlayerModule.play();
        BC_videoPlayerModule.showVolumeControls(true);

    } catch (err) {
       console.log(err);
   }
}

/*!
 * hoverIntent v1.8.0 // 2014.06.29 // jQuery v1.9.1+
 * http://cherne.net/brian/resources/jquery.hoverIntent.html
 *
 * You may use hoverIntent under the terms of the MIT license. Basically that
 * means you are free to use hoverIntent as long as this header is left intact.
 * Copyright 2007, 2014 Brian Cherne
 */
(function($){$.fn.hoverIntent=function(handlerIn,handlerOut,selector){var cfg={interval:100,sensitivity:6,timeout:0};if(typeof handlerIn==="object"){cfg=$.extend(cfg,handlerIn)}else{if($.isFunction(handlerOut)){cfg=$.extend(cfg,{over:handlerIn,out:handlerOut,selector:selector})}else{cfg=$.extend(cfg,{over:handlerIn,out:handlerIn,selector:handlerOut})}}var cX,cY,pX,pY;var track=function(ev){cX=ev.pageX;cY=ev.pageY};var compare=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);if(Math.sqrt((pX-cX)*(pX-cX)+(pY-cY)*(pY-cY))<cfg.sensitivity){$(ob).off("mousemove.hoverIntent",track);ob.hoverIntent_s=true;return cfg.over.apply(ob,[ev])}else{pX=cX;pY=cY;ob.hoverIntent_t=setTimeout(function(){compare(ev,ob)},cfg.interval)}};var delay=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);ob.hoverIntent_s=false;return cfg.out.apply(ob,[ev])};var handleHover=function(e){var ev=$.extend({},e);var ob=this;if(ob.hoverIntent_t){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t)}if(e.type==="mouseenter"){pX=ev.pageX;pY=ev.pageY;$(ob).on("mousemove.hoverIntent",track);if(!ob.hoverIntent_s){ob.hoverIntent_t=setTimeout(function(){compare(ev,ob)},cfg.interval)}}else{$(ob).off("mousemove.hoverIntent",track);if(ob.hoverIntent_s){ob.hoverIntent_t=setTimeout(function(){delay(ev,ob)},cfg.timeout)}}};return this.on({"mouseenter.hoverIntent":handleHover,"mouseleave.hoverIntent":handleHover},cfg.selector)}})(jQuery);

function enablerInitHandler(){

    if(window.innerWidth > 960){
        $.ajax( {
            url: '//banners.compassion.com/feeds/video-1Child.php',
            method: 'GET',
            crossDomain: true,
            dataType: 'jsonp'
        } ).done( function( childInfo ) {
            
            if(childInfo[0].Gender == 'F'){
                he_or_she = 'She';
                him_or_her = 'her';
            }
    
            var country = childInfo[0].Location;
            var display_country = null;
            switch( country ){
                case 'Dominican Republic':
                case 'Philippines':
                    display_country = 'the ' + country;
                    break;
                default:
                    display_country = country;
            }
            
            // FONT SIZE CLASSES
            var fontSize = 
                childInfo[0].Name.length > 7 && childInfo[0].Name.length < 9 || childInfo[0].Location.length > 7 && childInfo[0].Location.length < 9 ? 'small' : 
                childInfo[0].Name.length >= 9 && childInfo[0].Name.length < 10 || childInfo[0].Location.length >= 9 && childInfo[0].Location.length < 10 ? 'smaller' : 
                childInfo[0].Name.length >= 10 || childInfo[0].Location.length >= 10 ? 'smallest' : 
                'normal';
    
            document.getElementById("child-image").innerHTML='<img src="'+childInfo[0].ChildThumb+'" alt="'+childInfo[0].Name+' needs your help.">';
            document.getElementById("child-info-left").innerHTML='<p class="uppercase '+fontSize+'">This is <strong>'+childInfo[0].Name+'</strong></p>';
            document.getElementById("child-info-right").innerHTML='<p class="uppercase"><span class="banner_moving_text">'+he_or_she+' <strong>is hungry</strong></span><span class="hover"><strong>Hover to meet '+him_or_her+'</strong></p>';
            document.getElementById("child-info").innerHTML='<p class="big"><strong>My name is '+childInfo[0].Name+'</strong></p><p><strong>I\'m a '+childInfo[0].ChildAgeInYears+'-year-old '+childInfo[0].GenderLong+' from '+display_country+', and have been waiting '+childInfo[0].DaysWaiting+' days for a sponsor</strong></p><p>Make me part of your family, and help me reach my God-given potential. <strong><em>You</em></strong> can help transform my community.</p>';;
            document.getElementById("sponsor-button").innerHTML='<a href="'+childInfo[0].Link+'&referer=150812"></a>';
            document.getElementById("modal-sponsor-button").innerHTML='<a href="'+childInfo[0].Link+'&referer=150812"></a>';
            document.getElementById("video").innerHTML='<div class="fluid-video"><object id="myExperience' + childInfo[0].BrightCoveVideoID + '" class="BrightcoveExperience" style="padding-bottom:10px;"><param name="bgcolor" value="#FFFFFF" /><param name="width" value="300" /><param name="height" value="169" /><param name="playerID" value="' + childInfo[0].BrightCoveVideoID +'" /><param name="playerKey" value="AQ~~,AAADcS-2MOk~,TWDAHmx8NWRZ8yPlSlApYDZyfzRf5u3A" /><param name="isVid" value="true" /><param name="isUI" value="true" /><param name="dynamicStreaming" value="true" /><param name="@videoPlayer" value="' + childInfo[0].BrightCoveVideoID + '" /><<param name="templateLoadHandler" value="onTemplateLoaded" /><param name="templateReadyHandler" value="onTemplateReady" /></object></div>';
            brightcove.createExperiences();
        } );
    }
};

(function($){
    if(window.innerWidth > 960){
        $( document ).ready( function() {
            $( '#banner-hover' ).detach().appendTo('body');
            $('body').append('<div class="banner-hover-overlay"></div>');

            $( '#child-info-right' ).hoverIntent( {
                over: showModal,
                out: function(){},
                interval: 500,
                timeout: 1000,
            });
            
            function showModal(){
                if(video_is_ready){
                    $( '#banner-hover, .banner-hover-overlay' ).addClass('displayed');
                    BC_videoPlayerModule.play();
                }
            }
    
            $( '.close-banner' ).on( 'click', function() {
                $( '#banner-hover, .banner-hover-overlay' ).removeClass('displayed');
                BC_videoPlayerModule.pause();
            } );

        } );
    
        $( window ).load( function() {
    
    
            //  TEXT ANIMATION CHAIN
            $('.banner_moving_text').queue('animateText', function() {
                var self = this;
                setTimeout(function() {
                    $(self).dequeue('animateText');
                }, 2000);
            });
    
            $('.banner_moving_text').queue('animateText', function() {
                $(this).fadeOut('500', function() {
                    $(this).html(he_or_she + ' <strong>is poor</strong>').fadeIn();
                });
                var self = this;
                setTimeout(function() {
                    $(self).dequeue('animateText');
                }, 2000);
            });
            $('.banner_moving_text').queue('animateText', function() {
                $(this).fadeOut('500', function() {
                    $(this).addClass('smaller').html(he_or_she + ' <strong>has real dreams</strong>').fadeIn();
                });
                var self = this;
                setTimeout(function() {
                    $(self).dequeue('animateText');
                }, 2000);
            });
            $('.banner_moving_text').queue('animateText', function() {
                $(this).fadeOut('500', function() {
                    $(this).removeClass('smaller').html(he_or_she + ' <strong class="yellow">needs you</strong>').fadeIn();
                });
            });
            $('.banner_moving_text').dequeue('animateText');
    
        } );
    }
})(jQuery);
enablerInitHandler();