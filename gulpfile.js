var gulp = require('gulp');
var autoprefixer = require('gulp-autoprefixer');
var less = require('gulp-less');
var minifyCss = require('gulp-minify-css');
var concatCss = require('gulp-concat-css');
var concatJs = require('gulp-concat');
var uglify = require('gulp-uglify');

gulp.task('css', function() {
    return gulp.src('less/com-styles.less')
        .pipe(less())
        .pipe(autoprefixer('last 5 versions'))
        .pipe(concatCss('com-styles.min.css'))
        .pipe(minifyCss())
        .pipe(gulp.dest('./'))
});

gulp.task('scripts', function() {
    return gulp.src('scripts/com-sponsor.js')
        .pipe(concatJs('com-sponsor.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./'))
});

gulp.task('default', ['css', 'scripts']);