#Compassion Sponsor Me Script

##For Those Using this Script on Their Site
This script REQUIRES jQuery to function properly. If you already have jQuery installed on your site or, at minimum the page you want to display this code on, then you can skip this step. Otherwise, you will need to include jQuery.
```
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
```

With jQuery installed and loaded, you may then simply copy and paste the Compassion script below.
```
<div id="compassion-banner1234">
    <script type="text/javascript" src="//banner-host.dev/com-sponsor.min.js"></script>
</div>
```

##For Developers Modifying this Script
###Built Using
* jQuery
* Compassion CDN
* Gulp
* LESS

This project utilizes [Gulp.js](http://gulpjs.com/) and expects that it is already installed in your development environment. *Note: Gulp relies on [Node.JS](https://nodejs.org/). If you are unfamilar with Gulp, I recommend [this brief tutorial](https://laracasts.com/lessons/gulp-this).

####Gulp Packages Being Used
* gulp-autoprefixer
* gulp-less
* gulp-minify-css
* gulp-concat-css
* gulp-concat
* gulp-uglify